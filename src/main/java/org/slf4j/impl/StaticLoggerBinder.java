package org.slf4j.impl;

import org.seppiko.glf.slf4j.SLF4JLoggerFactory;
import org.slf4j.ILoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;

/**
 * @author Leonard Woo
 */
public class StaticLoggerBinder implements LoggerFactoryBinder {

  private static StaticLoggerBinder SINGLETON = new StaticLoggerBinder();

  public static StaticLoggerBinder getSingleton() {
    return SINGLETON;
  }

  private ILoggerFactory loggerFactory;
  private String clazzName;

  private StaticLoggerBinder() {
    loggerFactory = new SLF4JLoggerFactory();
    clazzName = SLF4JLoggerFactory.class.getName();
  }

  @Override
  public ILoggerFactory getLoggerFactory() {
    return loggerFactory;
  }

  @Override
  public String getLoggerFactoryClassStr() {
    return clazzName;
  }
}
