/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.slf4j;

import org.seppiko.glf.api.MarkerFactory;
import org.slf4j.IMarkerFactory;
import org.slf4j.Marker;

/**
 * @author Leonard Woo
 */
public class SLF4JMarkerFactory implements IMarkerFactory {

  private org.seppiko.glf.api.IMarkerFactory glfMarkerFactory;

  public SLF4JMarkerFactory() {
    glfMarkerFactory = MarkerFactory.getIMarkerFactory();
  }

  @Override
  public Marker getMarker(String name) {
    return (Marker) glfMarkerFactory.getMarker(name);
  }

  @Override
  public boolean exists(String name) {
    return glfMarkerFactory.exists(name);
  }

  @Override
  public boolean detachMarker(String name) {
    return glfMarkerFactory.detachMarker(name);
  }

  @Override
  public Marker getDetachedMarker(String name) {
    return (Marker) glfMarkerFactory.getDetachedMarker(name);
  }
}
