/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.slf4j;


import org.seppiko.glf.api.LoggerFactory;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

/**
 * @author Leonard Woo
 */
public class SLF4JLoggerFactory implements ILoggerFactory {

  private org.seppiko.glf.api.ILoggerFactory glfLoggerFactory;

  public SLF4JLoggerFactory() {
    glfLoggerFactory = LoggerFactory.getLoggerFactory();
  }

  @Override
  public Logger getLogger(String name) {
    return (Logger) glfLoggerFactory.getLogger(name);
  }
}
