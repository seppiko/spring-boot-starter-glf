/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.osgi;

import java.util.Properties;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceFactory;
import org.osgi.service.log.LogService;

/**
 * @author Leonard Woo
 */
public class Activator implements BundleActivator {

  @Override
  public void start(BundleContext bundleContext) throws Exception {
    Properties props = new Properties();
    props.put("description", "An GLF LogService implementation.");
    ServiceFactory factory = new LogServiceFactory();
    bundleContext.registerService(LogService.class.getName(), factory, props);
  }

  @Override
  public void stop(BundleContext bundleContext) throws Exception {
    //
  }
}
