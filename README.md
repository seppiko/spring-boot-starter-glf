# GLF for Spring boot 2.x

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/org.seppiko.glf/spring-boot-starter-glf/badge.svg)](https://maven-badges.herokuapp.com/maven-central/org.seppiko.glf/spring-boot-starter-glf)

GLF for Spring boot 2.x logger, Include glf-slf4j-impl and osgi-over-glf.

```xml
<dependency>
  <groupId>org.seppiko.glf</groupId>
  <artifactId>spring-boot-starter-glf</artifactId>
  <version>${seppiko-glf.version}</version>
</dependency>
```

## glf-slf4j-impl
Convert SLF4J API 1.7 to GLF implementation.

## osgi-over-glf
An implementation for OSGi log

## License
This project is under version 2.0 of the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100px"></a>
