/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.osgi;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;
import org.osgi.service.log.LogService;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;

/**
 * @author Leonard Woo
 */
public class LogServiceImpl implements LogService {

  private final Logger delegate;

  public LogServiceImpl(Bundle bundle) {
    String name = bundle.getSymbolicName();
    Version version = bundle.getVersion();
    if (version == null) {
      version = Version.emptyVersion;
    }
    delegate = LoggerFactory.getLogger(name + '.' + version);
  }

  @Override
  public void log(int level, String message) {
    switch (level) {
      case LOG_DEBUG:
        delegate.debug(message);
        break;
      case LOG_INFO:
        delegate.info(message);
        break;
      case LOG_WARNING:
        delegate.warn(message);
        break;
      case LOG_ERROR:
        delegate.error(message);
        break;
      default:
        delegate.error("OSGi called an invalid level.");
        break;
    }
  }

  @Override
  public void log(int level, String message, Throwable cause) {
    switch (level) {
      case LOG_DEBUG:
        delegate.debug(message, cause);
        break;
      case LOG_ERROR:
        delegate.error(message, cause);
        break;
      case LOG_INFO:
        delegate.info(message, cause);
        break;
      case LOG_WARNING:
        delegate.warn(message, cause);
        break;
      default:
        delegate.error("OSGi called an invalid level.");
        break;
    }
  }

  @Override
  public void log(ServiceReference sr, int level, String message) {
    switch (level) {
      case LOG_DEBUG:
        if (delegate.isDebugEnabled()) {
          delegate.debug(createMessage(sr, message));
        }
        break;
      case LOG_ERROR:
        if (delegate.isErrorEnabled()) {
          delegate.error(createMessage(sr, message));
        }
        break;
      case LOG_INFO:
        if (delegate.isInfoEnabled()) {
          delegate.info(createMessage(sr, message));
        }
        break;
      case LOG_WARNING:
        if (delegate.isWarnEnabled()) {
          delegate.warn(createMessage(sr, message));
        }
        break;
      default:
        delegate.error("OSGi called an invalid level.");
        break;
    }
  }

  @Override
  public void log(ServiceReference sr, int level, String message, Throwable cause) {
    switch (level) {
      case LOG_DEBUG:
        if (delegate.isDebugEnabled()) {
          delegate.debug(createMessage(sr, message), cause);
        }
        break;
      case LOG_ERROR:
        if (delegate.isErrorEnabled()) {
          delegate.error(createMessage(sr, message), cause);
        }
        break;
      case LOG_INFO:
        if (delegate.isInfoEnabled()) {
          delegate.info(createMessage(sr, message), cause);
        }
        break;
      case LOG_WARNING:
        if (delegate.isWarnEnabled()) {
          delegate.warn(createMessage(sr, message), cause);
        }
        break;
      default:
        delegate.error("OSGi called an invalid level.");
        break;
    }
  }

  private String createMessage(ServiceReference sr, String message) {
    StringBuilder output = new StringBuilder();
    if (sr != null) {
      output.append('[').append(sr.toString()).append(']');
    } else {
      output.append("[UNKNOWN]");
    }
    output.append(message);
    return output.toString();
  }

}
