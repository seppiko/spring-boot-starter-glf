# spring-boot-starter-glf

## 1.3.RELEASE 20201129
1. Fixed bug
2. Update GLF API

## 1.2.RELEASE 20201012
1. Implementation slf4j-glf-impl
2. Implementation osgi-over-glf
