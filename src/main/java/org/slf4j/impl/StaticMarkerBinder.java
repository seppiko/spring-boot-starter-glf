package org.slf4j.impl;

import org.seppiko.glf.slf4j.SLF4JMarkerFactory;
import org.slf4j.IMarkerFactory;
import org.slf4j.spi.MarkerFactoryBinder;

/**
 * @author Leonard Woo
 */
public class StaticMarkerBinder implements MarkerFactoryBinder {

  private static StaticMarkerBinder SINGLETON = new StaticMarkerBinder();

  public static StaticMarkerBinder getInstance() {
    return SINGLETON;
  }

  private IMarkerFactory markerFactory;
  private String clazzName;

  private StaticMarkerBinder() {
    markerFactory = new SLF4JMarkerFactory();
    clazzName = SLF4JMarkerFactory.class.getName();
  }

  @Override
  public IMarkerFactory getMarkerFactory() {
    return markerFactory;
  }

  @Override
  public String getMarkerFactoryClassStr() {
    return clazzName;
  }
}
